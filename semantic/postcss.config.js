module.exports = {
  plugins: {
    'postcss-preset-env': {
      // This little line means that we can import all the custom properties
      // and have fallback values in IE11 – which is great.
      // And since we don't rely on node's module resolution code, also a
      // bit brittle.
      importFrom:
        'node_modules/@ta-interaktiv/semantic-ui/semantic/dist/components/site.css'
    },
    autoprefixer: { grid: true },
    cssnano: {
      preset: [
        'default',
        {
          discardComments: { removeAll: true }
        }
      ]
    }
  }
}
