/*******************************
         Site Overrides
*******************************/

.ui.header .sub.header {
  font-weight: 500;
  line-height: 1.25em;
  margin-top: @10px;

  //Meta Header
  &.meta {
    color: @lightTextColor;
    font-weight: 500;
  }
}

.ui.header h1 {
  color: var(--text-color);
  font-weight: 700;
  line-height: 1.25em;
  margin-top: @10px;
  margin: 0;
  padding: 0;
  font-size: 4.2rem;
  line-height: calc(4.2rem * 1.061);
  @media screen and (max-width: 599px) {
    font-size: 2.005rem;
    line-height: calc(2.005rem * 1.125);
  }
}

.ui.header h2 {
  color: var(--text-color);
  font-family: @uiFont;
  font-weight: 100;
  font-size: 1.4rem;
  line-height: calc(1.4rem * 1.45);
  margin-top: .55555556rem;
  @media screen and (max-width: 599px) {
    font-size: 1.06rem;
    line-height: calc(1.06rem * 1.45);
  }
}

// Variation: Infographic Header

.infographic.infographic.infographic .ui.header,
.ui.infographic.infographic.infographic.header {
  font-family: @chartFont;
  font-weight: 700;

  &.huge, &.large {
    font-weight: 200;
  }

  .sub.header {
    font-family: @chartFont;
  }
}

// Sub Heading
.ui.sub.header {
  letter-spacing: 0.03em;
  opacity: 0.85;
  font-weight: @subHeadingFontWeight !important; // override for large and huge sub heading sizes
}

// Variation: Dividing Header
.ui.dividing.header {
  padding-bottom: 0;
  padding-top: @dividedBorderPadding;
  border-top: @dividedBorder;
  border-bottom: none;

  .sub.header {
    padding-bottom: 0;
  }

  .icon {
    margin-bottom: 0;
  }

  &.inverted {
    border-bottom-color: transparent;
    border-top-color: @invertedDividedBorderColor;
  }
}

// Sizing: Less extreme scaling on mobile devices
@media all and (max-width: @tabletBreakpoint) {

  .ui.header {
    .sub.header {
      line-height: 1.2em;
    }
  }

  h1.ui.header {
    font-size: @mobileH1;

    .sub.header {
      font-size: @large;
    }

  }

  h2.ui.header {
    font-size: @mobileH2;

    .sub.header {
      font-size: @large;
    }
  }

  h3.ui.header {
    font-size: @mobileH3;

    .sub.header {
      font-size: @large;
    }
  }

  h4.ui.header {
    font-size: @mobileH4;
  }

  h5.ui.header {
    font-size: @mobileH5;
  }
}

/*-------------------
       Attached
--------------------*/
.ui.attached.header {
  border: none;
  border-top: @attachedBorder;
}

.ui.top.attached.header {
  border-top: none;
}

.ui.red.segment,
.ui.orange.segment,
.ui.olive.segment,
.ui.yellow.segment,
.ui.green.segment,
.ui.teal.segment,
.ui.blue.segment,
.ui.violet.segment,
.ui.purple.segment,
.ui.pink.segment,
.ui.brown.segment,
.ui.grey.segment {
  .ui.attached.header:not(.inverted) {
    background-color: @veryVeryStrongTransparentWhite;
    border-top: none;
    margin-top: 1px;
  }
}

.ui.inverted.segment {
  .ui.attached.header:not(.inverted) {
    color: @textColor;
  }
}
