/*******************************
         Site Overrides
*******************************/


// Additional styles
@import "taTypography";
@import "../elements/caption";
@import "inverted";
@import "globalVisibility";

// Save some useful variables as CSS variables for use in overwriting CSS files
:root {
  // Font stacks
  --ui-font-stack: @uiFont;
  --text-font-stack: @textFont;
  --display-font-stack: @headerFont;
  --chart-font-stack: @chartFont;
  --infographic-font-stack: @chartFont;
  
  // Breakpoints
  // Will keep this information, even though it currently can't be used in a media query
  --largest-mobile-screen: @largestMobileScreen;
  --tablet-breakpoint: @tabletBreakpoint;
  --largest-tablet-screen: @largestTabletScreen;
  --computer-breakpoint: @computerBreakpoint;
  --largest-small-monitor: @largestSmallMonitor;
  --large-monitor-breakpoint: @largeMonitorBreakpoint;
  --largest-large-monitor: @largestLargeMonitor;
  --widescreen-monitor-breakpoint: @widescreenMonitorBreakpoint;
  
  // Colors
  --primary: @primaryColor;
  --secondary: @secondaryColor;
  --red: @red;
  --orange: @orange;
  --yellow: @yellow;
  --olive: @olive;
  --green: @green;
  --teal: @teal;
  --blue: @blue;
  --violet: @violet;
  --purple: @purple;
  --pink: @pink;
  --brown: @brown;
  --grey: @grey;
  --black: @black;
  
  // Light Colors
  // These are light colors as defined by Marina Bräm. 
  // They correspond to background colors in Semantic UI.
  --light-red: @redBackground;
  --light-orange: @orangeBackground;
  --light-yellow: @yellowBackground;
  --light-olive: @oliveBackground;
  --light-green: @greenBackground;
  --light-teal: @tealBackground;
  --light-blue: @blueBackground;
  --light-violet: @violetBackground;
  --light-purple: @purpleBackground;
  --light-pink: @pinkBackground;
  --light-brown: @brownBackground;
  
  // Text Colors
  --red-text-color: @redTextColor;
  --orange-text-color: @orangeTextColor;
  --yellow-text-color: @yellowTextColor;
  --olive-text-color: @oliveTextColor;
  --green-text-color: @greenTextColor;
  --teal-text-color: @tealTextColor;
  --blue-text-color: @blueTextColor;
  --violet-text-color: @blueTextColor;
  --purple-text-color: @purpleTextColor;
  --pink-text-color: @pinkTextColor;
  --brown-text-color: @brownTextColor;
  --dark-text-color: @darkTextColor;
  --text-color: @textColor;
  --muted-text-color: @mutedTextColor;
  --light-text-color: @lightTextColor;
  
  // Alpha Colors
  --subtle-transparent-black: @subtleTransparentBlack;
  --transparent-black: @transparentBlack;
  --strong-transparent-black: @strongTransparentBlack;
  --very-strong-transparent-black: @veryStrongTransparentBlack;
  --subtle-transparent-white: @subtleTransparentWhite;
  --transparent-white: @transparentWhite;
  --strong-transparent-white: @strongTransparentWhite;
  --off-white: @offWhite;
  --dark-white: @darkWhite;
  --mid-white: @midWhite;
  --light-grey: #E7F1F6;
  --mid-grey: #CDD2D3;
}

.inverted {
  --primary: @lightPrimaryColor;
  --secondary: @lightSecondaryColor;
  --red: @lightRed;
  --orange: @lightOrange;
  --yellow: @lightYellow;
  --olive: @lightOlive;
  --green: @lightGreen;
  --teal: @lightTeal;
  --blue: @lightBlue;
  --violet: @lightViolet;
  --purple: @lightPurple;
  --pink: @lightPink;
  --brown: @lightBrown;
  --grey: @lightGrey;
  --black: @lightBlack;
  
  // Text colors
  --dark-text-color: @invertedTextColor;
  --text-color: @invertedTextColor;
  --muted-text-color: @invertedMutedTextColor;
  --light-text-color: @invertedLightTextColor;

  // Alpha colors
  --subtle-transparent-black: @subtleTransparentWhite;
  --transparent-black: @transparentWhite;
  --strong-transparent-black: @strongTransparentWhite;
  --very-strong-transparent-black: @strongTransparentWhite;
  --subtle-transparent-white: @subtleTransparentBlack;
  --transparent-white: @transparentBlack;
  --strong-transparent-white: @strongTransparentBlack;
}